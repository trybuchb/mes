class Element
{
    constructor(id, lb, rb, tr, tl, c, k, ro, height, lenght, alfa, temp)
    {
        this.id = id;
        this.lb = lb;
        this.rb = rb;
        this.tr = tr;
        this.tl = tl;
        this.nodes = [lb, rb, tr, tl];
        // H
        this.ksi = [];
        this.eta = [];
        this.Xp = [];
        this.Yp = [];
        this.N1 = [];
        this.N2 = [];
        this.N3 = [];
        this.N4 = [];
        this.funkcjeKsztaltuEta = [];
        this.funkcjeKsztaltuKsi = [];
        this.Jakobians = [];
        this.JakobiansDet = [];
        this.JakobiansInverse = [];
        this.punktyCalkowaniaX = [];
        this.punktyCalkowaniaY = [];
        this.pochodneX = [];
        this.pochodneY = [];
        this.pochodneXT = [];
        this.pochodneYT = [];
        this.conductivity = k;
        this.specificHeat = c;
        this.density = ro;
        this.pochodneSumaK = [];
        this.H = [];
        this.edgeHeight = height;
        this.edgeLenght = lenght;
        this.alfa = alfa;
        this.tempOtoczenia = temp;
        // C
        this.pc1 = [];
        this.pc2 = [];
        this.pc3 = [];
        this.pc4 = [];
        this.C = [];
        // warunki brzegowe
        this.ksiBrzegowe = [-1 / Math.sqrt(3), 1 / Math.sqrt(3)];
        this.N1Brzegowe = [0.5 * (1 - this.ksiBrzegowe[0]), 0.5 * (1 - this.ksiBrzegowe[1])];
        this.N2Brzegowe = [0.5 * (1 + this.ksiBrzegowe[0]), 0.5 * (1 + this.ksiBrzegowe[1])];
        this.edges = [false, false, false, false]; // [dół, prawo, góra, lewo]
        this.edgesArray = [];
        //local H
        this.localH = [];
        //vector P
        this.vectorP = [];
    }

    calculateNs()
    {

        this.ksi = [-1 / Math.sqrt(3), 1 / Math.sqrt(3), 1 / Math.sqrt(3), -1 / Math.sqrt(3)];
        this.eta = [-1 / Math.sqrt(3), -1 / Math.sqrt(3), 1 / Math.sqrt(3), 1 / Math.sqrt(3)];

        for (let i = 0; i < 4; i++) {
            this.N1[i] = 0.25 * (1 - this.ksi[i]) * (1 - this.eta[i]);
            this.N2[i] = 0.25 * (1 + this.ksi[i]) * (1 - this.eta[i]);
            this.N3[i] = 0.25 * (1 + this.ksi[i]) * (1 + this.eta[i]);
            this.N4[i] = 0.25 * (1 - this.ksi[i]) * (1 + this.eta[i]);
        }

        for (let i = 0; i < 4; i++) {
            this.Xp[i] = this.N1[i] * this.nodes[0].x + this.N2[i] * this.nodes[1].x + this.N3[i] * this.nodes[2].x + this.N4[i] * this.nodes[3].x;
            this.Yp[i] = this.N1[i] * this.nodes[0].y + this.N2[i] * this.nodes[1].y + this.N3[i] * this.nodes[2].y + this.N4[i] * this.nodes[3].y;
        }

    }

    calculateFukcjeKsztaltuEtaKsi()
    {
        for (let i = 0; i < 4; i++) {
            var Eta0 = -0.25 * (1 - this.eta[i]);
            var Eta1 = 0.25 * (1 - this.eta[i]);
            var Eta2 = 0.25 * (1 + this.eta[i]);
            var Eta3 = -0.25 * (1 + this.eta[i]);
            this.funkcjeKsztaltuEta.push([Eta0, Eta1, Eta2, Eta3]);

            var Ksi0 = -0.25 * (1 - this.ksi[i]);;
            var Ksi1 = -0.25 * (1 + this.ksi[i]);;
            var Ksi2 = 0.25 * (1 + this.ksi[i]);;
            var Ksi3 = 0.25 * (1 - this.ksi[i]);;
            this.funkcjeKsztaltuKsi.push([Ksi0, Ksi1, Ksi2, Ksi3]);
        }
    }

    calculateJakobians()
    {
        var Jako = [];
        for (let i = 0; i < 4; i++) {
            Jako = [0, 0, 0, 0];
            for (let j = 0; j < 4; j++) {
                Jako[0] += this.funkcjeKsztaltuEta[i][j] * this.nodes[j].x;
            }
            for (let j = 0; j < 4; j++) {
                Jako[1] += this.funkcjeKsztaltuEta[i][j] * this.nodes[j].y;
            }
            for (let j = 0; j < 4; j++) {
                Jako[2] += this.funkcjeKsztaltuKsi[i][j] * this.nodes[j].x;
            }
            for (let j = 0; j < 4; j++) {
                Jako[3] += this.funkcjeKsztaltuKsi[i][j] * this.nodes[j].y;
            }
            this.Jakobians.push(Jako);
        }

        for (let i = 0; i < 4; i++) {
            this.JakobiansDet[i] = this.Jakobians[i][0] * this.Jakobians[i][3] - this.Jakobians[i][1] * this.Jakobians[i][2];
        }

        for (let j = 0; j < 4; j++) {
            Jako = [0, 0, 0, 0];
            for (let i = 0; i < 4; i++) {
                Jako[0] = this.Jakobians[i][3] / this.JakobiansDet[i];
                Jako[1] = this.Jakobians[i][1] / this.JakobiansDet[i];
                Jako[2] = this.Jakobians[i][2] / this.JakobiansDet[i];
                Jako[3] = this.Jakobians[i][0] / this.JakobiansDet[i];
            }
            this.JakobiansInverse.push(Jako);
        }
    }

    calculatePunktyCalkowaniaXY()
    {
        var X, Y = [];
        for (let j = 0; j < 4; j++) {
            X = [];
            Y = [];
            for (let i = 0; i < 4; i++) {
                X.push(this.JakobiansInverse[i][0] * this.funkcjeKsztaltuEta[i][j] + this.JakobiansInverse[i][1] * this.funkcjeKsztaltuKsi[i][j])
                Y.push(this.JakobiansInverse[i][2] * this.funkcjeKsztaltuEta[i][j] + this.JakobiansInverse[i][3] * this.funkcjeKsztaltuKsi[i][j])
            }
            this.punktyCalkowaniaX.push(X);
            this.punktyCalkowaniaY.push(Y);
        }
    }

    calculateCrossArray(a, b, c, d)
    {  //{dN/dx}{dN/dx}T   / {dN/dy}{dN/dy}T
        var numbers = [a, b, c, d];
        var array = [];
        for (let i = 0; i < 4; i++) {
            var a = [];
            for (let j = 0; j < 4; j++) {
                a.push(numbers[i] * numbers[j]);
            }
            array.push(a);
        }
        return array;
    }

    calculatePochodne()
    {
        var array = [];
        for (let i = 0; i < 4; i++) {
            array = this.calculateCrossArray(this.punktyCalkowaniaX[0][i], this.punktyCalkowaniaX[1][i], this.punktyCalkowaniaX[2][i], this.punktyCalkowaniaX[3][i]);
            this.pochodneX.push(array);
            array = [0];
        }
        for (let i = 0; i < 4; i++) {
            array = this.calculateCrossArray(this.punktyCalkowaniaY[0][i], this.punktyCalkowaniaY[1][i], this.punktyCalkowaniaY[2][i], this.punktyCalkowaniaY[3][i]);
            this.pochodneY.push(array);
            array = [0];
        }

        this.pochodneXT = this.pochodneX;
        this.pochodneYT = this.pochodneY;
        this.pochodneSumaK = this.pochodneY;

        for (let i = 0; i < 4; i++) {
            for (let j = 0; j < 4; j++) {
                for (let k = 0; k < 4; k++) {
                    this.pochodneXT[i][j][k] *= this.JakobiansDet[i];
                    this.pochodneYT[i][j][k] *= this.JakobiansDet[i];
                    this.pochodneSumaK[i][j][k] = this.conductivity * (this.pochodneXT[i][j][k] + this.pochodneYT[i][j][k]);
                }
            }
        }

    }

    calculateH()
    {
        var array = [];
        for (let i = 0; i < 4; i++) {
            array = [];
            for (let j = 0; j < 4; j++) {
                array.push(this.pochodneSumaK[0][i][j] + this.pochodneSumaK[1][i][j] + this.pochodneSumaK[2][i][j] + this.pochodneSumaK[3][i][j]);
            }
            this.H.push(array);
        }
    }

    calculatePc()
    {
        var array = [];
        for (let i = 0; i < 4; i++) {
            array = [];
            for (let j = 0; j < 4; j++) {
                array.push(this.N1[i] * this.N1[j] * this.JakobiansDet[0] * this.specificHeat * this.density);
            }
            this.pc1.push(array);
        }

        for (let i = 0; i < 4; i++) {
            array = [];
            for (let j = 0; j < 4; j++) {
                array.push(this.N2[i] * this.N2[j] * this.JakobiansDet[0] * this.specificHeat * this.density);
            }
            this.pc2.push(array);
        }

        for (let i = 0; i < 4; i++) {
            array = [];
            for (let j = 0; j < 4; j++) {
                array.push(this.N3[i] * this.N3[j] * this.JakobiansDet[0] * this.specificHeat * this.density);
            }
            this.pc3.push(array);
        }

        for (let i = 0; i < 4; i++) {
            array = [];
            for (let j = 0; j < 4; j++) {
                array.push(this.N4[i] * this.N4[j] * this.JakobiansDet[0] * this.specificHeat * this.density);
            }
            this.pc4.push(array);
        }

    }

    calculateC()
    {
        var array = [];
        for (let i = 0; i < 4; i++) {
            array = [];
            for (let j = 0; j < 4; j++) {
                array.push(this.pc1[i][j] + this.pc2[i][j] + this.pc3[i][j] + this.pc4[i][j]);
            }
            this.C.push(array);
        }
    }

    checkIfEdges()
    {
        if (this.lb.edge == true && this.rb.edge == true) {
            this.edges[0] = true;
        }
        if (this.rb.edge == true && this.tr.edge == true) {
            this.edges[1] = true;
        }
        if (this.tr.edge == true && this.tl.edge == true) {
            this.edges[2] = true;
        }
        if (this.tl.edge == true && this.lb.edge == true) {
            this.edges[3] = true;
        }

    }

    calculateMagicArray(edgeLenght)
    {
        var magic1 = [], magic2 = [], magic = [], array1, array2;
        var detJ = edgeLenght / 2;
        for (let i = 0; i < 2; i++) {
            array1 = [];
            array2 = [];
            for (let j = 0; j < 2; j++) {
                array1.push(this.N1Brzegowe[i] * this.N1Brzegowe[j] * this.alfa);
                array2.push(this.N2Brzegowe[i] * this.N2Brzegowe[j] * this.alfa);
            }
            magic1.push(array1);
            magic2.push(array2);
        }
        for (let i = 0; i < 2; i++) {
            array1 = [];
            for (let j = 0; j < 2; j++) {
                array1.push((magic1[i][j] + magic2[i][j]) * detJ)
            }
            magic.push(array1);
        }
        return magic;
    }

    calculateEdgesArray()
    {
        var array = [], magic = [];
        for (let i = 0; i < 4; i++) {
            array = [];
            for (let j = 0; j < 4; j++) {
                array.push(0)
            }
            this.edgesArray.push(array);
        }

        if (this.edges[0] == true) {
            magic = this.calculateMagicArray(this.edgeLenght)
            this.edgesArray[0][0] += magic[0][0];
            this.edgesArray[0][1] += magic[0][1];
            this.edgesArray[1][0] += magic[1][0];
            this.edgesArray[1][1] += magic[1][1];
        }
        if (this.edges[1] == true) {
            magic = this.calculateMagicArray(this.edgeHeight)
            this.edgesArray[1][1] += magic[0][0];
            this.edgesArray[1][2] += magic[0][1];
            this.edgesArray[2][1] += magic[1][0];
            this.edgesArray[2][2] += magic[1][1];
        }
        if (this.edges[2] == true) {
            magic = this.calculateMagicArray(this.edgeLenght)
            this.edgesArray[2][2] += magic[0][0];
            this.edgesArray[2][3] += magic[0][1];
            this.edgesArray[3][2] += magic[1][0];
            this.edgesArray[3][3] += magic[1][1];
        }
        if (this.edges[3] == true) {
            magic = this.calculateMagicArray(this.edgeHeight)
            this.edgesArray[0][0] += magic[0][0];
            this.edgesArray[0][3] += magic[0][1];
            this.edgesArray[3][0] += magic[1][0];
            this.edgesArray[3][3] += magic[1][1];
        }

    }

    calculateLocalH()
    {
        var array = [];
        for (let i = 0; i < 4; i++) {
            array = [];
            for (let j = 0; j < 4; j++) {
                array.push(this.H[i][j] + this.edgesArray[i][j])
            }
            this.localH.push(array);
        }
    }

    calculateP()
    {
        var pcv1 = [], pcv2 = [], vsuma = [], detJ = this.edgeLenght / 2;


        pcv1[0] = this.N1Brzegowe[0] * this.tempOtoczenia * this.alfa;
        pcv1[1] = this.N1Brzegowe[1] * this.tempOtoczenia * this.alfa;

        pcv2[0] = this.N2Brzegowe[0] * this.tempOtoczenia * this.alfa;
        pcv2[1] = this.N2Brzegowe[1] * this.tempOtoczenia * this.alfa;

        vsuma[0] = (pcv1[0] + pcv2[0]) * detJ;
        vsuma[1] = (pcv1[1] + pcv2[1]) * detJ;

        for (let i = 0; i < 4; i++) {
            this.vectorP.push(0);
        }

        if (this.edges[0] == true) {
            this.vectorP[0] += vsuma[0]
            this.vectorP[1] += vsuma[1]
        }
        if (this.edges[1] == true) {
            this.vectorP[1] += vsuma[0]
            this.vectorP[2] += vsuma[1]
        }
        if (this.edges[2] == true) {
            this.vectorP[2] += vsuma[0]
            this.vectorP[3] += vsuma[1]
        }
        if (this.edges[3] == true) {
            this.vectorP[3] += vsuma[0]
            this.vectorP[0] += vsuma[1]
        }

    }

}
module.exports = Element;