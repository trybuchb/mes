const Grid = require("./Grid");
const Data = require("./data.json")
const math = require('mathjs')
const gauss = require('./gauss');

var grid = new Grid(Data)

grid.calculateGlobalH();
grid.calculateGlobalH2(); // H + brzegowe
grid.calculateGlobalP();
grid.calculateGlobalC();
grid.calculateGlobalCPT(); // [H] = [H]+[C]/dT
grid.calculateStep(grid.temp);

function calculateStep(temp, C, simulationStep)
{
    var bb = math.matrix(C);
    bb = math.divide(bb, simulationStep)
    const dd = math.matrix(temp);
    const step = math.multiply(bb, dd)
    return step.toArray();
}


var constantP = JSON.parse(JSON.stringify(grid.globalP));
var step = JSON.parse(JSON.stringify(grid.step));
var C = JSON.parse(JSON.stringify(grid.globalC));
var simulationStep = JSON.parse(JSON.stringify(grid.simulationStep));
var constantH = JSON.parse(JSON.stringify(grid.globalCPT));

var P;
var T1;

for (let j = grid.simulationStep; j <= grid.simulationTime; j += grid.simulationStep) {

    P = [];
    for (let i = 0; i < (grid.nodesHeight * grid.nodesLengh); i++) {
        P[i] = (constantP[i] + step[i]);
    }

    T1 = gauss(constantH, P);

    var maxTemp = Math.max.apply(Math, T1);
    var minTemp = Math.min.apply(Math, T1);

    console.log(j + " time, minTemp: " + minTemp + " ,maxTemp: " + maxTemp);

    constantH = JSON.parse(JSON.stringify(grid.globalCPT));
    step = calculateStep(T1, C, simulationStep);

}
