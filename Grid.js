const Node = require("./Node");
const Element = require("./Element");

class Grid
{
    constructor(data)
    {
        this.initialTemp = data.initialTemp;
        this.simulationTime = data.simulationTime;
        this.simulationStep = data.simulationStep;
        this.ambientTemp = data.ambientTemp;
        this.alfa = data.alfa;
        this.height = data.H;                  //wysokosc w metrach
        this.lenght = data.B;                  //szerokosc w metrach
        this.nodesHeight = data.N_H;           //ilosc nodow na wysokosc
        this.nodesLengh = data.N_B;            //ilosc nodow na szerokosc
        this.specificHeat = data.specificHeat;   // c
        this.conductivity = data.conductivity;   // K
        this.density = data.density;             // ro

        this.nodes = [];            //tablica nodow
        this.elements = [];         //tablica elementow
        this.amountOfNodes = this.nodesLengh * this.nodesHeight;
        this.amountOfElements;
        this.generateNodes();
        this.generateElements();
        this.H();
        this.C();
        this.edges();
        this.globalH = [];
        this.globalH2 = [];
        this.globalP = [];
        this.globalC = [];
        this.globalCPT = [];
        this.step = [];
        this.temp = []
        this.temp0();
    }

    getGlobalP()
    {
        return this.globalP
    }
    temp0()
    {
        for (let i = 0; i < (this.nodesHeight * this.nodesLengh); i++) {
            this.temp.push(this.initialTemp);
        };
    }

    H()
    {
        this.elements.forEach(element =>
        {
            element.calculateNs();
            element.calculateFukcjeKsztaltuEtaKsi();
            element.calculateJakobians();
            element.calculatePunktyCalkowaniaXY();
            element.calculatePochodne();
            element.calculateH();
        });
    }

    C()
    {
        this.elements.forEach(element =>
        {
            element.calculatePc();
            element.calculateC();
        });
    }

    edges()
    {
        this.elements.forEach(element =>
        {
            element.checkIfEdges();
            element.calculateEdgesArray();
            element.calculateLocalH();
            element.calculateP();
        });
    }

    generateNodes()
    {
        let nh = this.nodesHeight;
        let nl = this.nodesLengh;
        let stepH = this.height / (nh - 1);
        let stepL = this.lenght / (nl - 1);
        var edge;

        for (let i = 0; i < nl; i++) {
            for (let j = 0; j < nh; j++) {
                let nodeNr = i * nh + j;
                if (i == 0 || j == 0 || i == (nl - 1) || j == (nh - 1)) {
                    edge = true;
                } else {
                    edge = false;
                }
                this.nodes[nodeNr] = new Node(nodeNr, i * stepL, j * stepH, this.initialTemp, edge);
            }

        }
    }

    generateElements()
    {
        let nh = this.nodesHeight;
        let nl = this.nodesLengh;
        let currentElement = 0;
        for (let i = 0; i < nl - 1; i++) {
            for (let j = 0; j < nh - 1; j++) {
                this.elements[currentElement] = new Element(
                    currentElement, this.nodes[i * nh + j], this.nodes[(i + 1) * nh + j],
                    this.nodes[(i + 1) * nh + j + 1], this.nodes[i * nh + j + 1],
                    this.specificHeat, this.conductivity, this.density,
                    this.height / (this.nodesHeight - 1), this.lenght / (this.nodesLengh - 1),
                    this.alfa, this.ambientTemp);
                currentElement++;
            }

        }
        this.amountOfElements = currentElement;
    }

    calculateGlobalH()
    {

        let nh = this.nodesHeight;
        let nl = this.nodesLengh;

        var array = [];
        for (let i = 0; i < (nh * nh); i++) {
            array = [];
            for (let j = 0; j < (nl * nl); j++) {
                array.push(0)
            }
            this.globalH.push(array);
        }

        for (let i = 0; i < (nh - 1) * (nl - 1); i++) {
            for (let j = 0; j < 4; j++) {
                for (let k = 0; k < 4; k++) {
                    let i1 = this.elements[i].nodes[j].id;
                    let i2 = this.elements[i].nodes[k].id;
                    this.globalH[i1][i2] += this.elements[i].H[j][k];
                }
            }
        }
    }

    calculateGlobalH2()
    {

        let nh = this.nodesHeight;
        let nl = this.nodesLengh;

        var array = [];
        for (let i = 0; i < (nh * nh); i++) {
            array = [];
            for (let j = 0; j < (nl * nl); j++) {
                array.push(0)
            }
            this.globalH2.push(array);
        }

        for (let i = 0; i < (nh - 1) * (nl - 1); i++) {
            for (let j = 0; j < 4; j++) {
                for (let k = 0; k < 4; k++) {
                    let i1 = this.elements[i].nodes[j].id;
                    let i2 = this.elements[i].nodes[k].id;
                    this.globalH2[i1][i2] += this.elements[i].localH[j][k];
                }
            }
        }
    }

    calculateGlobalC()
    {

        let nh = this.nodesHeight;
        let nl = this.nodesLengh;

        var array = [];
        for (let i = 0; i < (nh * nh); i++) {
            array = [];
            for (let j = 0; j < (nl * nl); j++) {
                array.push(0)
            }
            this.globalC.push(array);
        }

        for (let i = 0; i < (nh - 1) * (nl - 1); i++) {
            for (let j = 0; j < 4; j++) {
                for (let k = 0; k < 4; k++) {
                    let i1 = this.elements[i].nodes[j].id;
                    let i2 = this.elements[i].nodes[k].id;
                    this.globalC[i1][i2] += this.elements[i].C[j][k];
                }
            }
        }
    }

    calculateGlobalP()
    {
        let nh = this.nodesHeight;
        let nl = this.nodesLengh;

        for (let I = 0; I < (nh * nl); I++) {
            this.globalP.push(0);
        }

        for (let i = 0; i < (nh - 1) * (nl - 1); i++) {
            for (let j = 0; j < 4; j++) {
                let i1 = this.elements[i].nodes[j].id;

                this.globalP[i1] += this.elements[i].vectorP[j];
            }
        }

    }

    calculateGlobalCPT()  //matrix H + matrix C
    {
        let nh = this.nodesHeight;
        let nl = this.nodesLengh;

        this.globalCPT = JSON.parse(JSON.stringify(this.globalH2));

        for (let i = 0; i < (nh * nl); i++) {
            for (let j = 0; j < (nh * nl); j++) {
                this.globalCPT[i][j] += (this.globalC[i][j] / this.simulationStep);
            }
        }
    }

    calculateStep(temp)
    {
        let nh = this.nodesHeight;
        let nl = this.nodesLengh;
        var step = [], C = this.globalC;


        for (let I = 0; I < (nh * nl); I++) {
            step.push(0);
        }

        for (let i = 0; i < (nh * nl); i++) {
            for (let j = 0; j < (nh * nl); j++) {
                step[i] += C[i][j] / this.simulationStep * temp[i];
            }
        }

        this.step = step;
        return step;
    }
}
module.exports = Grid;
