class Node
{
    constructor(id, x, y, t, edge)
    {
        this.id = id;
        this.x = x;
        this.y = y;
        this.t = t;
        this.edge = edge;
    }
}
module.exports = Node;
